//
//  TenyMalagasyApp.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 18/06/2024.
//

import SwiftUI

@main
struct TenyMalagasyApp: App {
    
    @StateObject var navigationStore = NavigationStore()
    
    var body: some Scene {
            WindowGroup {
                /*let repository = CarsRepositoryImpl(dataSource: CarDataSource())
                let getCarsUseCase = GetCarsUseCase(repository: repository)
                let viewModel = CarsViewModel(getCarsUseCase: getCarsUseCase)
                CarsView(viewModel: viewModel)*/
                
                MainView().environmentObject(navigationStore)
            }
        }
}


