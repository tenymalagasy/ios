//
//  CarDataSource.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 18/06/2024.
//

import Foundation

class CarDataSource {
    func getCars(completion: @escaping ([Car]) -> Void) {
        let cars = [
            Car(name: "Toyota"),
            Car(name: "Honda"),
            Car(name: "Ford")
        ]
        completion(cars)
    }
}
