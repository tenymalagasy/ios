//
//  CarsRepositoryImpl.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 18/06/2024.
//

import Foundation

class CarsRepositoryImpl: CarsRepository {
    private let dataSource: CarDataSource

    init(dataSource: CarDataSource) {
        self.dataSource = dataSource
    }

    func getCars(completion: @escaping ([Car]) -> Void) {
        dataSource.getCars(completion: completion)
    }
}
