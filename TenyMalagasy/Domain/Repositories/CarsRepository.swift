//
//  CarsRepository.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 18/06/2024.
//

import Foundation

protocol CarsRepository {
    func getCars(completion: @escaping ([Car]) -> Void)
}
