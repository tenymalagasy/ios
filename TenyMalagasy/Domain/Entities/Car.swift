//
//  Car.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 18/06/2024.
//

import Foundation

struct Car: Identifiable {
    let id = UUID()
    let name: String
}
