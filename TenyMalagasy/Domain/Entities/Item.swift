//
//  Item.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 21/06/2024.
//

import Foundation

struct Item: Identifiable{
    let id: UUID
    let name: String
}

