//
//  GetCarsUseCase.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 18/06/2024.
//

import Foundation

class GetCarsUseCase {
    private let repository: CarsRepository

    init(repository: CarsRepository) {
        self.repository = repository
    }

    func execute(completion: @escaping ([Car]) -> Void) {
        repository.getCars(completion: completion)
    }
}
