//
//  GetItemsUseCase.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 21/06/2024.
//

import Foundation

class GetItemsUseCase {
    func execute() -> [Item] {
        return [
            Item(id: UUID(), name: "Item 1"),
            Item(id: UUID(), name: "Item 2")
        ]
    }
}

