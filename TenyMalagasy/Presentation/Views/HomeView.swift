//
//  HomeView.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 21/06/2024.
//

import SwiftUI

struct HomeView: View {
    
    @EnvironmentObject var navigationStore: NavigationStore
    
    var body: some View {
        VStack {
            
            Spacer()
            
            Text("Exemple de merde")
                .padding(.top, 130)
            
            Image(systemName: "star.fill") // Vous pouvez remplacer par votre propre image
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)
                .padding()
            
            Spacer()
            
            Button(action: {
                navigationStore.currentScreen = .presentation
            }) {
                Text("CONTINUER")
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
                    .padding(.horizontal)
            }
            .padding(.bottom, 30) // Ajustez la valeur en fonction de vos besoins
        }
        .navigationBarTitle("Home", displayMode: .inline)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
