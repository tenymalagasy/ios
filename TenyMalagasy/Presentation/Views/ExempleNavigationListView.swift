//
//  ExempleNavigationList.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 22/06/2024.
//

import SwiftUI

struct ExempleNavigationListView: View {
    @ObservedObject var viewModel: ExempleNavigationListViewModel
    @EnvironmentObject var navigationStore: NavigationStore
    
    var body: some View {
        List(viewModel.items) { item in
            Button(action: {
                navigationStore.currentScreen = .profile(item: item)
            }) {
                Text(item.name +  " - ID: \(item.id.uuidString)")
            }
        }
        .onAppear {
            viewModel.loadItems()
        }
        .navigationBarTitle("Home", displayMode: .inline)
    }
}
