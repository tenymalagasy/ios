//
//  PresentationView.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 22/06/2024.
//

import SwiftUI

struct PresentationView: View {
    
    var body: some View {
        VStack {
            Spacer()
            
            Image(systemName: "star.fill") // Vous pouvez remplacer par votre propre image
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)
                .padding()
            
            Spacer()
            
            Button(action: {
                print("Button tapped!")
            }) {
                Text("Press Me")
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
                    .padding(.horizontal)
            }
            .padding(.bottom, 30) // Ajustez la valeur en fonction de vos besoins
        }
        .navigationBarTitle("Home", displayMode: .inline)
    }
}

struct PresentationView_Previews: PreviewProvider {
    static var previews: some View {
        PresentationView()
    }
}
