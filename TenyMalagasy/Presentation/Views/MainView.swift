//
//  MainView.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 21/06/2024.
//

import Foundation
import SwiftUI

struct MainView: View {
    @EnvironmentObject var navigationStore: NavigationStore
    
    var body: some View {
        NavigationView {
            ZStack {
                switch navigationStore.currentScreen {
                case .list:
                    ExempleNavigationListView(viewModel: ExempleNavigationListViewModel(getItemsUseCase: GetItemsUseCase()))
                case .home:
                    HomeView()
                case .presentation:
                    PresentationView().transition(.move(edge: .trailing))
                case .profile(let item):
                    ProfileView(viewModel: ProfileViewModel(item: item))
                }
            }
            .animation(.easeInOut, value: navigationStore.currentScreen)
        }
        .navigationViewStyle(.stack)
        //.navigationViewStackTransition(.slide)
    }
}

