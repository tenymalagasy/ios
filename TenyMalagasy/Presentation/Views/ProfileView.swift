//
//  ProfileView.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 21/06/2024.
//

import Foundation
import SwiftUI

struct ProfileView: View {
    @ObservedObject var viewModel: ProfileViewModel
    @EnvironmentObject var navigationStore: NavigationStore
    
    var body: some View {
        Text("Item: \(viewModel.item.name)")
            .navigationBarTitle("Profil", displayMode: .inline)
            .navigationBarItems(leading: Button(action: {
                        navigationStore.currentScreen = .home
                    }) {
                        HStack {
                            Image(systemName: "chevron.left")
                            Text("Back")
                        }
                    })
    }
}
