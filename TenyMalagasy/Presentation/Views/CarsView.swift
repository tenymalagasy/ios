//
//  CarsView.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 18/06/2024.
//

import SwiftUI

struct CarsView: View {
    @ObservedObject var viewModel: CarsViewModel

        var body: some View {
            List(viewModel.cars) { car in
                Text(car.name)
            }
            .onAppear {
                viewModel.getCars()
            }
        }
}


