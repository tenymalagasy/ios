//
//  ProfileViewModel.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 21/06/2024.
//

import Foundation
import Combine

class ProfileViewModel: ObservableObject {
    @Published var item: Item
    
    init(item: Item) {
        self.item = item
    }
}
