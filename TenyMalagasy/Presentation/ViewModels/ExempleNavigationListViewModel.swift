//
//  ExempleNavigationListViewModel.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 22/06/2024.
//

import Foundation
import Combine

class ExempleNavigationListViewModel: ObservableObject {
    private let getItemsUseCase: GetItemsUseCase
    
    @Published var items: [Item] = []
    
    init(getItemsUseCase: GetItemsUseCase) {
        self.getItemsUseCase = getItemsUseCase
    }
    
    func loadItems() {
        items = getItemsUseCase.execute()
    }
}
