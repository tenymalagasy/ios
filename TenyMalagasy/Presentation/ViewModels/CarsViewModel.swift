//
//  CarsViewModel.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 18/06/2024.
//

import Foundation

class CarsViewModel: ObservableObject {
    @Published var cars: [Car] = []
    private let getCarsUseCase: GetCarsUseCase

    init(getCarsUseCase: GetCarsUseCase) {
        self.getCarsUseCase = getCarsUseCase
    }

    func getCars() {
        getCarsUseCase.execute { cars in
            DispatchQueue.main.async {
                self.cars = cars
            }
        }
    }
}

