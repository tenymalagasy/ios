//
//  NavigationStore.swift
//  TenyMalagasy
//
//  Created by Mandimby Andraina on 21/06/2024.
//

import Combine

class NavigationStore: ObservableObject {
    enum Screen: Equatable {
        
        case list
        case home
        case presentation
        case profile(item: Item)
        
        static func == (lhs: Screen, rhs: Screen) -> Bool {
            switch (lhs, rhs) {
            case (.home, .home),
                 (.presentation, .presentation),
                 (.list, .list):
                return true
            case let (.profile(item1), .profile(item2)):
                return item1.id == item2.id
            default:
                return false
            }
        }
    }
    
    @Published var currentScreen: Screen = .home
}
